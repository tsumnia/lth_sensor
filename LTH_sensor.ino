#include "DHT.h"
//#include "JeeLib.h" //Used to lower power consumption

#define DHTPIN 6
#define LED 3
#define LIGHT A0
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600); 
  Serial.println("DHTxx test!");
  pinMode(LED, OUTPUT);
  pinMode(LIGHT, INPUT);
 
  dht.begin();
}

void loop() {
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  float x = dht.readTemperature();
  float t = convert_C_to_F(x);
  int l = readLumen();
  
  LED_ON();
  
  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    Serial.println("Failed to read from DHT");
  } else {
    Serial.print("H: "); 
    Serial.print(h);
    Serial.print("%\t");
    Serial.print("T: "); 
    Serial.print(t);
    Serial.print("*F");
    Serial.print("\t");
    Serial.print("L: "); 
    Serial.println(l);
  }
  
  LED_OFF();
  
  delay(10000);
  //Sleepy::loseSomeTime(10000); Outputs weird text on serial, not sure why?
}

void LED_ON() {
  digitalWrite(LED, HIGH);
}

void LED_OFF() {
  digitalWrite(LED, LOW);
}

int readLumen() {
  int lumen = analogRead(LIGHT);
  return lumen;
}

float convert_C_to_F(float celcius) {
  return celcius * 1.8 + 32;
}
