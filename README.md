LTH_sensor
==========

A Light, Temperature, and Humidity Sensor for Arduino

#### Dependencies
* [DHT Library](https://github.com/adafruit/DHT-sensor-library) - Used to read the Temperature and Humidity reads
* [JeeLib](https://github.com/jcw/jeelib) - Currently not used due to weird reads from Serial, but created for lower power consumption